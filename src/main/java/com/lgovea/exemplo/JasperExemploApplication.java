package com.lgovea.exemplo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JasperExemploApplication {

	public static void main(String[] args) {
		SpringApplication.run(JasperExemploApplication.class, args);
	}

}
