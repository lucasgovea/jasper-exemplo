package com.lgovea.exemplo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lgovea.exemplo.domain.Usuario;
import com.lgovea.exemplo.service.JasperReportService;
import com.lgovea.exemplo.service.UsuarioService;

import net.sf.jasperreports.engine.JRException;

@RestController
public class MainController {

	@Autowired
	public UsuarioService usuarioService;
	
	@Autowired
	public JasperReportService jasperService;

	@ResponseBody
	@GetMapping("/")
	public ResponseEntity<byte[]> gerarPdf() {
		
		List<Usuario>usuarios = usuarioService.obterTodos();
		String fileName = "relatorio.jrxml";
		try {
			
			byte[] bytes = jasperService.generatePDFReport(fileName, null, usuarios);
			return ResponseEntity.ok()
					//Specify content type as PDF
					.header("Content-Type", "application/pdf; charset=UTF-8")
					//Tell browser to display PDF if it can
					.header("Content-Disposition", "inline; filename=\"arquivo.pdf\"").body(bytes);
		} catch (JRException e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Falha ao gerar relatorio".getBytes());
		}
	}
}
