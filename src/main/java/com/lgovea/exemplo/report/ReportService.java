//package com.lgovea.exemplo.report;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.springframework.stereotype.Service;
//
//import br.com.lgovea.mille.domain.ItemOrcamento;
//import br.com.lgovea.mille.domain.ItemPedido;
//import br.com.lgovea.mille.domain.Orcamento;
//import br.com.lgovea.mille.domain.Pedido;
//import br.com.lgovea.mille.domain.TipoExtensaoArquivo;
//import br.com.lgovea.mille.report.CustomJRDataSource;
//import br.com.lgovea.mille.service.ImpressaoService;
//import net.sf.jasperreports.engine.JRException;
//import net.sf.jasperreports.engine.JasperCompileManager;
//import net.sf.jasperreports.engine.JasperExportManager;
//import net.sf.jasperreports.engine.JasperFillManager;
//import net.sf.jasperreports.engine.JasperPrint;
//import net.sf.jasperreports.engine.JasperReport;
//import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
//import net.sf.jasperreports.engine.design.JasperDesign;
//import net.sf.jasperreports.engine.export.JRXlsExporter;
//import net.sf.jasperreports.engine.xml.JRXmlLoader;
//import net.sf.jasperreports.export.SimpleExporterInput;
//import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
//import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
//
//@Service
//public class ReportService {
//	
//	private static final String PATH_RELATORIOS = "jasper"+File.separatorChar;
//	
//	private static final String PATH_CABECALHO_RELATORIOS = "static"+File.separatorChar+"images"+File.separatorChar+"logo-preto.png";
//	
//	
//	
//	public byte[] gerarPedidoParaImpressao(TipoExtensaoArquivo extensao, Pedido pedido) throws JRException, IOException {
//		
//		Map<String,Object> parameters = new HashMap<String,Object>();
//		
//		parameters.put("SUB_RELATORIO", PATH_RELATORIOS+"subrelatorio_itens.jasper");
//		
//		List<ItemPedido> itens = new ArrayList<ItemPedido>();
//		for (ItemPedido item : pedido.getItens()) {
//			itens.add(item);
//		}
//		JRBeanCollectionDataSource itensDataSource = new JRBeanCollectionDataSource(itens);
//		
//		parameters.put("ITENS_SUBRELATORIO", itensDataSource);
//		parameters.put("IMAGEM_CABECALHO", this.getClass().getClassLoader().getResourceAsStream(PATH_CABECALHO_RELATORIOS));
//		
//		
//		List<Pedido> pedidos = new ArrayList<Pedido>();
//		pedidos.add(pedido);
//		
//		
//		return this.gerarPedidoParaExtensao(extensao, "pedido.jrxml", pedidos,  parameters);
//			
//	}
//	
//	public byte[] gerarSolicitacaoOrcamentoParaImpressao(TipoExtensaoArquivo extensao, Orcamento orcamento) throws JRException, IOException {
//		Map<String,Object> parameters = new HashMap<String,Object>();
//		
//		parameters.put("SUB_RELATORIO", PATH_RELATORIOS+"subrelatorio_itensOrcamento.jasper");
//		
//		List<ItemOrcamento> itens = new ArrayList<ItemOrcamento>();
//		for (ItemOrcamento item : orcamento.getItensOrcamento()) {
//			itens.add(item);
//		}
//		JRBeanCollectionDataSource itensDataSource = new JRBeanCollectionDataSource(itens);
//		
//		parameters.put("ITENS_SUBRELATORIO", itensDataSource);
//		parameters.put("IMAGEM_CABECALHO", this.getClass().getClassLoader().getResourceAsStream(PATH_CABECALHO_RELATORIOS));
//		
//		
//		List<Orcamento> orcamentos = new ArrayList<Orcamento>();
//		orcamentos.add(orcamento);
//		
//		
//		return this.gerarPedidoParaExtensao(extensao, "orcamento.jrxml", orcamentos,  parameters);
//	}
//	
//	private byte[] gerarPedidoParaExtensao(TipoExtensaoArquivo extensao, String arquivoJasper, List listaResult, Map parameters) throws JRException{
//		if(extensao.equals(TipoExtensaoArquivo.EXTENSAO_PDF))
//			return listarPedidoPDF(arquivoJasper, listaResult,parameters);
//		if(extensao.equals(TipoExtensaoArquivo.EXTENSAO_XLS));
//			return listarPedidoXLS(arquivoJasper, listaResult,parameters);
//	}
//	
//	
//	private byte[] listarPedidoPDF(String arquivoJasper, List resultRelatorio,Map parameters) throws JRException{
//		
//		
//		InputStream input = this.getClass().getClassLoader().getResourceAsStream(PATH_RELATORIOS+arquivoJasper);
//        JasperDesign design = JRXmlLoader.load(input);
//		
//		JasperReport jasperReport = JasperCompileManager.compileReport(design);
//		
//		CustomJRDataSource dataSource = new CustomJRDataSource().initBy(resultRelatorio);
//		
//		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,dataSource);
//		
//		return JasperExportManager.exportReportToPdf(jasperPrint);
//	}
//	 
//
//
//	private byte[] listarPedidoXLS(String arquivoJasper, List resultRelatorio,Map parameters) throws JRException {
//		
//		InputStream input = this.getClass().getClassLoader().getResourceAsStream(PATH_RELATORIOS+arquivoJasper);
//        JasperDesign design = JRXmlLoader.load(input);;
//		
//		JasperReport jasperReport = JasperCompileManager.compileReport(design);
//		
//		CustomJRDataSource dataSource = new CustomJRDataSource().initBy(resultRelatorio);
//		
//		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,dataSource);
//		
//		ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
//		JRXlsExporter exporter = new JRXlsExporter();
//		exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReport));
//		SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
//		configuration.setOnePagePerSheet(false);
//		configuration.setDetectCellType(true);
//		configuration.setIgnoreGraphics(false);
//		configuration.setCollapseRowSpan(false);
//		configuration.setRemoveEmptySpaceBetweenRows(true);
//		configuration.setWhitePageBackground(false);
//		configuration.setShowGridLines(false);
//		exporter.setConfiguration(configuration);
//        exporter.exportReport();
//        return xlsReport.toByteArray();
//	}
//}
