package com.lgovea.exemplo.service;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.lgovea.exemplo.report.CustomJRDataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@Service
public class JasperReportService {
	
	private static final String PATH_RELATORIOS = "jasper"+File.separatorChar;
	
	private static final String PATH_CABECALHO_RELATORIOS = "static"+File.separatorChar+"images"+File.separatorChar+"logo.png";	

	public byte[] generatePDFReport(String inputFileName, Map<String, Object> params, List dataSource) throws JRException {
		InputStream input = this.getClass().getClassLoader().getResourceAsStream(PATH_RELATORIOS+inputFileName);
        JasperDesign design = JRXmlLoader.load(input);
		
		JasperReport jasperReport = JasperCompileManager.compileReport(design);
		 
		if(params == null) {
			params = new HashMap<>();
		}
		params.put("SUB_RELATORIO", PATH_RELATORIOS+"subrelatorio.jasper");
		params.put("IMAGEM_CABECALHO", this.getClass().getClassLoader().getResourceAsStream(PATH_CABECALHO_RELATORIOS));
		
		CustomJRDataSource customDataSource = new CustomJRDataSource().initBy(dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, customDataSource);
		
		return JasperExportManager.exportReportToPdf(jasperPrint);

	}
}
