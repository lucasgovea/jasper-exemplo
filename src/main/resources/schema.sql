
CREATE TABLE usuario (
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(255) NOT NULL,
  profissao varchar(255) DEFAULT NULL,
  data_nascimento date NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE endereco (
  id int(11) NOT NULL AUTO_INCREMENT,
  logradouro varchar(255) NOT NULL,
  numero int(11) DEFAULT NULL,
  bairro varchar(255) NOT NULL,
  estado varchar(255) NOT NULL,
  complemento varchar(255) NOT NULL,
  cidade varchar(255) NOT NULL,
  id_usuario int(11) NOT NULL,
  PRIMARY KEY (id),
  foreign key (id_usuario) references usuario(id) 
);
